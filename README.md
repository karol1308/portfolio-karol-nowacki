# Karol Nowacki

Moje portfolio zawierające przypadki testowe, projekty i inne owoce mojej pracy testera.
## O mnie
Cześć, nazywam się Karol Nowacki i jestem początkującym testerem oprogramowania. Chętnie podzielę się w tym miejscu z Tobą moim doświadczeniem.

![profile](img/profile.jpeg)

</center>

## Testowanie
Moim zdaniem bez odpowiednio przeprowadzonych testów, nie można liczyć, że wydane oprogramowanie będzie poprawnie działać. Od zawsze lubię grzebać w plikach, patrzeć czy można coś "popsuć" albo czy autor aplikacji przewidział jakąś ścieżkę. Zdecydowałem się na pójście na całość i zostanie testerem oprogramowania i liczę, że moje umiejętności pomogą właśnie w Twoim zespole.

## Kurs Software Development Academy
Miałem przyjemność uczestniczyć w kursie "Tester Manualny" organizowanym przez Software Development Academy. Przez 105 godzin zajęć oraz wiele godzin poświęconych na pracę samodzielną zdobyłem wiedzę z następujących tematów:
<center>

![profile](img/tester_certificate.png)

</center>

Sprawnie będę poruszał się także w projektach zwinnych, dzięki zajęciom wprowadzających do metodyki Scrum:

<center>

![profile](img/scrum.png)

</center>

## Git 
W czasie kursu nauczyłem się nie tylko testować, ale rozwijałem swoje umiejętności w wielu kierunkach między innymi:

* Nauczyłem się pracy z Narzędziem GIT (oraz Gitlab)

## Certyfikat ISTQB
W czasie kursu przygotowywaliśmy się do egzaminu ISTQB Foundation Level. Obecnie jeszcze nie podszedłem do samego egzaminu, choć nie wykluczam tego w najbliższych miesącach



## Zadania, które wykonywałem w czasie kursu:
<center>

[Techniki Projektowania Testów](Techniki Projektowania Testów) |
[Zajęcia z Testowania](Zajęcia z Testowania) |
[Podstawy baz danych](Podstawy baz danych) |
[Wprowadzenie do programowania](Wprowadzenie do programowania) |

</center>

## Moje projekty

* Przypadki Testowe dla strony [PHPTravels](https://www.phptravels.net/)


</center>

## Zainteresowania
Poza testowaniem hoduję w formikarium kolonię mrówek. Testuję nie tylko oprogramowanie ale też ciekawe połączenia smaków w kuchni. Gotowanie to też mój sposób na relaks.

</center>

## Kontakt

Skontaktuj się ze mną mailowo: karol.nowacki.work@gmail.com

Linkedin: [Karol Nowacki](https://www.linkedin.com/in/karol-nowacki/)
